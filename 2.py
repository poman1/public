import socket
import threading

# Our thread class:
class ClientThread(threading.Thread):

    def __init__(self, conn, addr):
        self.conn = conn
        self.addr = addr
        threading.Thread.__init__(self)

    def run(self):
        data = conn.recv(1024)
        if data == 'close' or data == 'Close':
  self.close()
else
  self.send(data)
        if data == 'close': self.conn.close()
        self.conn.send(data)
        self.conn.close()

# Set up the server:
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('0.0.0.0', 2222))
s.listen(10)

# Have the server serve "forever":
while True:
    conn, addr = s.accept()
    ClientThread(conn, addr).start()